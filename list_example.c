#include "list.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct Name {
  char name [400];
  struct list_elem elem;
};
struct Name* make_name(char * name);
void print_list(struct list * l);

// would have to add more checking if it was not a dummy example
struct Name* make_name(char * name){
   struct Name * n = malloc(sizeof(struct Name));
   strcpy(n->name,name);
   return n;
}

// pass a pointer to be safe, although do not have to if you are making no modifications
void print_list(struct list* l){
  struct list_elem * e;
  printf("Printing a list of size %d\n", (int)list_size(l));
  for(e = list_begin(l); e != list_end(l); e = list_next(e)){
      struct Name * name = list_entry(e, struct Name, elem);
      printf("There is an element with a name of %s\n", name->name);
  }
  printf("The list was printed\n\n");

}
int main(){
    struct list example_list;
    // must initialize the list
    list_init(&example_list);

    print_list(&example_list);

    
    struct Name* one = make_name("Eric Williamson");
    list_push_back(&example_list,&one->elem);
    print_list(&example_list);



    list_push_front(&example_list,&make_name("Harrison Fang")->elem);
    print_list(&example_list);


    list_pop_front(&example_list);
    print_list(&example_list);

    return 0;
}
